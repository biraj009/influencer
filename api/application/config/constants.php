<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



/* ---------Site Settings-------- */
/* ------------------------------ */

/* Site Related Settings */
define('SITE_NAME', 'FSL11');
define('SITE_CONTACT_EMAIL', 'info@bfl.com');
define('MULTISESSION', true);
define('PHONE_NO_VERIFICATION', true);
define('DATE_FORMAT', "%Y-%m-%d %H:%i:%s"); /* dd-mm-yyyy */
define('SPORTS_FILE_PATH', FCPATH . 'uploads/sports.txt');
define('FOOTBALl_SPORTS_FILE_PATH', FCPATH . 'uploads/football_sports.txt');
define('SPORTS_API_NAME', 'CRICKETAPI');
define('FOOTBALL_SPORT_API_NAME', 'CRICKETAPI');

define('DEFAULT_SOURCE_ID', 1);
define('DEFAULT_DEVICE_TYPE_ID', 1);
define('DEFAULT_CURRENCY', 'Rs.');
define('REFERRAL_SIGNUP_BONUS', 50);
define('DEFAULT_PLAYER_CREDITS', 6.5);
define('DEFAULT_TIMEZONE', '+05:30');
define('TIMEZONE_DIFF_IN_SECONDS', 19800);

define('IS_VICECAPTAIN', true);
define('MATCH_TIME_IN_HOUR', 300);
/* for push Notification */
define('CHANNEL_NAME', 'Alert***');
define('ANDROID_SERVER_KEY', 'AIzaSyA6Skj80hswPdsg1P-sRnnowlEosizsWUU****');

/* Social */
define('FACEBOOK_URL', 'https://www.facebook.com/');
define('TWITTER_URL', 'https://twitter.com');
define('LINKEDIN_URL', 'https://www.linkedin.com/');
define('INSTAGRAM_URL', 'https://www.instagram.com/');

/* Entity Sports API Details */
define('SPORTS_API_URL_ENTITY', 'https://rest.entitysport.com');
define('SPORTS_API_ACCESS_KEY_ENTITY', 'f55cda5a0bc1ccf1badec616db39a827');
define('SPORTS_API_SECRET_KEY_ENTITY', 'a403e586ba951da7a18ab1778f8068a3');

//define('AUTH_TOKEN_CRIC91', 'GS.x5uQqeUyT3iY22LI2CRtVA.Fv-WlnD1Ku5cClAVCKaU4yRsJYVolutDGiRonTsKseg');
define('AUTH_TOKEN_PLAYERCASHOUT', 'GS2.zY3cEeBIisw9dpTvTmKP.CP-gpAw9XQWX73GTF3LCevAJd2nqvpEhIp4ESzQtmpC1V');
define('AUTH_TOKEN_GAME_SERVER', 'MW.95B34tMvS6wVfMNkgp6vZvBxfzhkk5QvEN2ZvYwE4rHzNJ8j7DmB5fQZy2LAbvKJ5zuJTW');

/* Cricket API Sports API Details */
define('FOOTBALL_SPORTS_API_URL_CRICKETAPI', 'https://api.footballapi.com');
define('FOOTBALL_SPORTS_API_ACCESS_KEY_CRICKETAPI', '1608cd76cddbd86dabee8ac834f12864');
define('FOOTBALL_SPORTS_API_SECRET_KEY_CRICKETAPI', 'd2528c2cc7c89b3ee0379008576f95fa');
define('FOOTBALL_SPORTS_API_APP_ID_CRICKETAPI', 'Fantasy Sports');
define('FOOTBALL_SPORTS_API_DEVICE_ID_CRICKETAPI', '***');

define('SPORTS_API_URL_CRICKETAPI', 'https://rest.cricketapi.com');
define('SPORTS_API_ACCESS_KEY_CRICKETAPI', '1fd962489eda80eeb05376ebb7fb17db');
define('SPORTS_API_SECRET_KEY_CRICKETAPI', '30d1d0e7068b41af8558a4d932e00060');
define('SPORTS_API_APP_ID_CRICKETAPI', 'Cricket99');
define('SPORTS_API_DEVICE_ID_CRICKETAPI', 'developer');

/* PayUMoney Details */
define('PAYUMONEY_MERCHANT_KEY', 'pUB61YeO');
define('PAYUMONEY_MERCHANT_ID', '6487911');
define('PAYUMONEY_SALT', 'KFB3aqMkP3');
define('PAYUMONEY_USERID', 'swapnil@games91.in');
define('PAYUMONEY_PASSWORD', 'Login@1234');
define('PAYUMONEY_AUTHURL', 'https://accounts.payu.in/oauth/token');
define('PAYUMONEY_CLIENTID', 'ccbb70745faad9c06092bb5c79bfd919b6f45fd454f34619d83920893e90ae6b');
define('PAYUMONEY_URL','https://payout.payumoney.com/payout');
define('PAYOUT_MERCHANTID', '1111259');

/* SMS API Details */
define('SMS_API_URL', 'https://login.bulksmsgateway.in/sendmessage.php');
define('SMS_API_USERNAME', '***');
define('SMS_API_PASSWORD', '***');

/* SENDINBLUE SMS API Details */
define('SENDINBLUE_SMS_API_URL', 'https://api.sendinblue.com/v3/transactionalSMS/sms');
define('SENDINBLUE_SMS_SENDER', 'EXACT11');
define('SENDINBLUE_SMS_API_KEY', 'xkeysib-******-72qcrDmbQ0HpGExS');


/* MSG91 SMS API Details */
// define('MSG91_AUTH_KEY', '273511AR202sqgYF5cbdd3eb');
define('MSG91_AUTH_KEY', '273511AObV1jwyud5cc067fd32323223');
define('MSG91_SENDER_ID', 'MCric91');
define('MSG91_FROM_EMAIL', 'info@Cric91.in');

define('POST_PICTURE_URL', BASE_URL . 'uploads/Post/');
define('SPORTS_TYPE', "PUBG");
define('AUTH_TOKEN_PLAYERCASHOUT' , 'GS2.zY3cEeBIisw9dpTvTmKP.CP-gpAw9XQWX73GTF3LCevAJd2nqvpEhIp4ESzQtmpC1V');
switch (ENVIRONMENT) {
    case 'local':
        /* Paths */
        define('SITE_HOST', 'http://localhost');
        define('ROOT_FOLDER', 'playercashout/');
        define('GAME_API_URL_CRIC91','http://13.234.19.187:3500');
        /* SMTP Settings */
        define('SMTP_PROTOCOL', 'smtp');
        define('SMTP_HOST', 'smtp.gmail.com');
        define('SMTP_PORT', '587');
        define('SMTP_USER', '');
        define('SMTP_PASS', '');
        define('SMTP_CRYPTO', 'tls'); /* ssl */

        /* From Email Settings */
        define('FROM_EMAIL', 'info@expertteam.in');
        define('FROM_EMAIL_NAME', SITE_NAME);

        /* No-Reply Email Settings */
        define('NOREPLY_EMAIL', SITE_NAME);
        define('NOREPLY_NAME', "info@expertteam.in");

        /* Site Related Settings */
        define('API_SAVE_LOG', false);
        define('CRON_SAVE_LOG', false);

        /* Paytm Details */
        define('PAYTM_MERCHANT_ID', 'SDsAag689**014846478');
        define('PAYTM_MERCHANT_KEY', 'qSAmxS#Mb**PmHiA');
        define('PAYTM_DOMAIN', 'securegw.paytm.in');
        define('PAYTM_INDUSTRY_TYPE_ID', 'Retail');
        define('PAYTM_WEBSITE_WEB', 'DEFAULT');
        define('PAYTM_WEBSITE_APP', 'DEFAULT');
        define('PAYTM_TXN_URL', 'https://' . PAYTM_DOMAIN . '/theia/processTransaction');
        define('PAYUMONEY_ACTION_KEY', 'https://secure.payu.in/_payment');

        /* PAYTM AUTOWITHDRAW */
        define('AUTO_WITHDRAWAL', true);
        define('PAYTM_MERCHANT_KEY_WITHDRAWAL', 'VJJmcZ*****TxRDD');
        define('PAYTM_MERCHANT_mId', '****');
        define('PAYTM_MERCHANT_GUID', 'bf106d****46b8968e322');
        define('PAYTM_SALES_WALLET_GUID', 'c2709a0f-*****9274cfa5a61');
        define('PAYTM_GRATIFICATION_URL', 'https://trust.paytm.in/wallet-web/salesToUserCredit'); // For Withdraw

        define('APP_PAYTM_MERCHANT_ID', 'SDsA****4846478');
        define('APP_PAYTM_MERCHANT_KEY', '****#MbPmHiA');
        define('APP_PAYTM_DOMAIN', 'securegw.paytm.in');
        define('APP_PAYTM_INDUSTRY_TYPE_ID', 'Retail');
        define('APP_PAYTM_WEBSITE_WEB', 'DEFAULT');
        define('APP_PAYTM_WEBSITE_APP', 'DEFAULT');
        define('APP_PAYTM_TXN_URL', 'https://' . APP_PAYTM_DOMAIN . '/theia/processTransaction');

        /* Razorpay Details */
        define('RAZORPAY_KEY_ID', '***');
        define('RAZORPAY_KEY_SECRET', '6p0CmdhT****AmmZBFC');

        break;
    case 'testing':

        /* Paths */
        define('SITE_HOST', 'http://192.168.1.251/');
        define('ROOT_FOLDER', 'localhost/');

        /* SMTP Settings */
        define('SMTP_PROTOCOL', 'smtp');
        define('SMTP_HOST', 'smtp.gmail.com');
        define('SMTP_PORT', '587');
        define('SMTP_USER', '');
        define('SMTP_PASS', '');
        define('SMTP_CRYPTO', 'tls'); /* ssl */

        /* From Email Settings */
        define('FROM_EMAIL', 'info@expertteam.in');
        define('FROM_EMAIL_NAME', SITE_NAME);

        /* No-Reply Email Settings */
        define('NOREPLY_EMAIL', SITE_NAME);
        define('NOREPLY_NAME', "info@expertteam.in");

        /* Site Related Settings */
        define('API_SAVE_LOG', false);
        define('CRON_SAVE_LOG', true);

        /* Paytm Details */
        define('PAYTM_MERCHANT_ID', 'Pfytge9****84428170');
        define('PAYTM_MERCHANT_KEY', '****%eV');
        define('PAYTM_DOMAIN', 'securegw-stage.paytm.in');
        define('PAYTM_INDUSTRY_TYPE_ID', 'Retail');
        define('PAYTM_WEBSITE_WEB', 'WEBSTAGING');
        define('PAYTM_WEBSITE_APP', 'APPSTAGING');
        define('PAYTM_TXN_URL', 'https://' . PAYTM_DOMAIN . '/theia/processTransaction');
        define('PAYUMONEY_ACTION_KEY', 'https://test.payu.in/_payment');

        /* Razorpay Details */
        define('RAZORPAY_KEY_ID', '***');
        define('RAZORPAY_KEY_SECRET', '6p0Cmd****AmmZBFC');
        break;
    case 'demo':
        /* Paths */
        define('GAME_API_URL_CRIC91', 'http://13.234.19.187:3500');
        define('SITE_HOST', 'https://tourneytest.games91.in/');
        define('ROOT_FOLDER', 'playercashout/');

        /* SMTP Settings */
        define('SMTP_PROTOCOL', 'smtp');
        define('SMTP_HOST', 'smtp.gmail.com');
        define('SMTP_PORT', '587');
        define('SMTP_USER', '');
        define('SMTP_PASS', '');
        define('SMTP_CRYPTO', 'tls'); /* ssl */

        /* From Email Settings */
        define('FROM_EMAIL', 'info@expertteam.in');
        define('FROM_EMAIL_NAME', SITE_NAME);

        /* No-Reply Email Settings */
        define('NOREPLY_EMAIL', SITE_NAME);
        define('NOREPLY_NAME', "info@expertteam.in");

        /* Site Related Settings */
        define('API_SAVE_LOG', false);
        define('CRON_SAVE_LOG', true);

        /* Paytm Details */
        define('PAYTM_MERCHANT_ID', 'Pfytg****28170');
        define('PAYTM_MERCHANT_KEY', '***%eV');
        define('PAYTM_DOMAIN', 'securegw-stage.paytm.in');
        define('PAYTM_INDUSTRY_TYPE_ID', 'Retail');
        define('PAYTM_WEBSITE_WEB', 'WEBSTAGING');
        define('PAYTM_WEBSITE_APP', 'APPSTAGING');
        define('PAYTM_TXN_URL', 'https://' . PAYTM_DOMAIN . '/theia/processTransaction');
        define('PAYUMONEY_ACTION_KEY', 'https://test.payu.in/_payment');

        /* Razorpay Details */
        define('RAZORPAY_KEY_ID', '***');
        define('RAZORPAY_KEY_SECRET', '6p0CmdhTR******mmZBFC');
        break;
    case 'production':
        /* Paths */
        define('GAME_API_URL_CRIC91', 'https://api.games91.in');
        define('SITE_HOST', 'https://tourney.games91.in/');
        define('ROOT_FOLDER', 'live/playercashout/');

        /* SMTP Settings */
        define('SMTP_PROTOCOL', 'smtp');
        define('SMTP_HOST', 'smtp.gmail.com');
        define('SMTP_PORT', '587');
        define('SMTP_USER', '');
        define('SMTP_PASS', '');
        define('SMTP_CRYPTO', 'tls'); /* ssl */

        /* From Email Settings */
        define('FROM_EMAIL', 'info@expertteam.in');
        define('FROM_EMAIL_NAME', SITE_NAME);

        /* No-Reply Email Settings */
        define('NOREPLY_EMAIL', SITE_NAME);
        define('NOREPLY_NAME', "info@expertteam.in");

        /* Site Related Settings */
        define('API_SAVE_LOG', false);
        define('CRON_SAVE_LOG', false);

        /* Paytm Details */
        define('PAYTM_MERCHANT_ID', 'SDsAag6846478*****');
        define('PAYTM_MERCHANT_KEY', 'qSAmx7TS#MbPmHiA****');
        define('PAYTM_DOMAIN', 'securegw.paytm.in');
        define('PAYTM_INDUSTRY_TYPE_ID', 'Retail');
        define('PAYTM_WEBSITE_WEB', 'DEFAULT');
        define('PAYTM_WEBSITE_APP', 'DEFAULT');
        define('PAYTM_TXN_URL', 'https://' . PAYTM_DOMAIN . '/theia/processTransaction');
        define('PAYUMONEY_ACTION_KEY', 'https://secure.payu.in/_payment');

        /* PAYTM AUTOWITHDRAW */
        define('AUTO_WITHDRAWAL', true);
        define('PAYTM_MERCHANT_KEY_WITHDRAWAL', 'VJJmcZ0Hs2iTxRDD******');
        define('PAYTM_MERCHANT_mId', 'FS11Pr32785972943695******');
        define('PAYTM_MERCHANT_GUID', 'bf106deb-e***46b8968e322');
        define('PAYTM_SALES_WALLET_GUID', 'c2709a0f-67a5****-a9274cfa5a61');
        define('PAYTM_GRATIFICATION_URL', 'https://trust.paytm.in/wallet-web/salesToUserCredit'); // For Withdraw

        define('APP_PAYTM_MERCHANT_ID', 'SDsAag68559014846478****');
        define('APP_PAYTM_MERCHANT_KEY', 'qSAmx7TS#MbPmHiA*****');
        define('APP_PAYTM_DOMAIN', 'securegw.paytm.in');
        define('APP_PAYTM_INDUSTRY_TYPE_ID', 'Retail');
        define('APP_PAYTM_WEBSITE_WEB', 'DEFAULT');
        define('APP_PAYTM_WEBSITE_APP', 'DEFAULT');
        define('APP_PAYTM_TXN_URL', 'https://' . APP_PAYTM_DOMAIN . '/theia/processTransaction');


        /* Razorpay Details */
        define('RAZORPAY_KEY_ID', 'rzp*****gspmKvxH***');
        define('RAZORPAY_KEY_SECRET', '6av****kdicl18****');
        break;
}

define('BASE_URL', SITE_HOST . ROOT_FOLDER . 'api/');
define('ASSET_BASE_URL', BASE_URL . 'asset/');
define('PROFILE_PICTURE_URL', BASE_URL . 'uploads/profile/picture');

