app.controller('PageController', function ($scope, $http, $timeout) {
    $scope.data.pageSize = 15;
    $scope.data.DEFAULT_CURRENCY = "INR";
    /*list append*/

    /*list*/
    $scope.applyFilter = function ()
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
    }


    $scope.getList = function () {
        if ($scope.data.listLoading || $scope.data.noRecords) return;
        $scope.data.listLoading = true;
        var data = 'SessionKey=' + SessionKey + '&ContestGUID=' + getQueryStringValue('ContestGUID') + '&Params=KillWinning,Kills,PubgUsername,PubgID,Username,PhoneNumber,ProfilePic,UserRank,TotalPoints,UserWinningAmount,UserTeamPlayers,UserTeamName&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&OrderBy=UserRank&Sequence=DESC'+ "&" + $('#filterForm').serialize();
        $http.post(API_URL + 'contest/getJoinedContestsUsersNew', data, contentType).then(function (response) {
            var response = response.data;
            if (response.ResponseCode == 200 && response.Data.Records) { /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                for (var i in response.Data.Records) {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }
                $scope.data.pageNo++;
            } else {
                $scope.data.noRecords = true;
            }
            $scope.data.listLoading = false;
        });
    }
    $scope.getContestDetail = function () {
        $scope.seriesDetail = {};
        if (getQueryStringValue('ContestGUID')) {
            var ContestGUID = getQueryStringValue('ContestGUID');
            $scope.AllContests = false;
        } else {
            var ContestGUID = '';
            $scope.AllContests = true;
        }
        $http.post(API_URL + 'contest/getGame', 'ContestGUID=' + ContestGUID + '&Params=IsWinningTransferred,IsWinningDistributeAmount,GameType,ContestID,IsRefund,WinningType,NoOfWinners,Kills,EntryFee,CustomizeWinning,RoomPassword,Map,RoomID,GameVersion,PerKill,GameSchedule,Status&SessionKey=' + SessionKey, contentType).then(function (response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.contestDetail = response.Data;
                var range = [];
                if($scope.contestDetail.WinningType != "PerKill"){
                    for(var i=1;i<=$scope.contestDetail.NoOfWinners;i++) {
                      range.push(i);
                    }
                    $scope.range = range;
                }
            }
        });
    }
    $scope.showUserList = function (row,key){
        $('#viewTeams_model').modal({show: true});
        $scope.UserPlayers = row.UserTeamPlayers;
        console.log(row.UserTeamPlayers);
    }
    $scope.UpdateKillPoints = function (row,key) {
        $scope.ContestID = row.ContestID;
        $scope.UserGUID = row.UserGUID;
        $scope.Kill = $("#TotalPoints_"+key).val();
        $scope.Rank = $("#TotalRanks_"+key).val();
        var data = 'SessionKey=' + SessionKey + '&ContestID=' + $scope.ContestID + '&UserGUID=' + $scope.UserGUID + '&Kills=' + $scope.Kill + "&Rank="+$scope.Rank;
        $http.post(API_URL + 'contest/UpdateKills', data, contentType).then(function (response) { 
             if (response.data.ResponseCode == 200) {
                    alertify.success(response.data.Message);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
             }else{
                alertify.error(response.data.Message);
             }
        }); 
    }
    $scope.updateContestWinning = function (ContestID){
        var data = 'SessionKey=' + SessionKey + '&ContestID=' + ContestID;
        $http.post(API_URL + 'contest/updateContestWinning', data, contentType).then(function (response) { 
            alertify.success("Successfully Updated");
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        });  
    }
    $scope.transferContestWinning = function (ContestID){
        var data = 'SessionKey=' + SessionKey + '&ContestID=' + ContestID;
        $http.post(API_URL + 'contest/contestWinningWalletCredit', data, contentType).then(function (response) { 
            alertify.success("Successfully Updated");
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        });  
    }
    $scope.transferContestRefund = function (ContestID){
        var data = 'SessionKey=' + SessionKey + '&ContestID=' + ContestID;
        $http.post(API_URL + 'contest/refundAmountCancelContest', data, contentType).then(function (response) { 
            alertify.success("Successfully Updated");
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        });  
    }
    $scope.ExportExcelMatches = function (ContestID) {
        var FromDate = $('#FromDate').val();
        var ToDate = $('#ToDate').val();
        window.open(API_URL + 'utilities/getContestsExportsWithWinning?ContestID=' + ContestID , "_blank");
    }
}); 