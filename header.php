<!DOCTYPE html>
<html lang="en">
<head>
  <title> Battlefight </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
  <link rel="icon" type="png/jpg" href="assets/img/fav.png"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="assets/css/slick.css" rel="stylesheet">
  <link href="assets/css/slick-theme.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body>
<nav class="navbar navbar-expand-lg  site_header fixed-top">
  <div class="container d-block">
    <div class="row align-items-center">
      <div class="col-lg-2 col-sm-12 order-1 ">
    <a class="navbar-brand site-logo" href="index.html"><!-- <img src="assets/img/logo.png" class="img-fluid" alt="logo"> --> Logo </a>
    <button class="navbar-toggler collapsed" onclick="myFunction(this)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
    </button>
  </div>

  </div>
</nav>

<main