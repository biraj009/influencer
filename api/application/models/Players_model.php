<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Players_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->model('Utility_model');
    }




    function getPlayersList() {
        $ApiUrl = GAME_API_URL_CRIC91.'/player/cashout/order?page=1&limit=100&type=Cash%20Out&orderStatus=Pending';
        // http://13.234.19.187:3500/player/cashout/order?page=1&limit=100&type=Cash Out&orderStatus=Pending

        /* Excecute Curl */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer ' .AUTH_TOKEN_PLAYERCASHOUT;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $result = curl_exec($ch);
        $result = json_decode($result, TRUE);
        
        if (curl_errno($ch)) {
            return FALSE;
        }
        curl_close($ch);

        return $result;
    }

    function savePlayerCashoutTemp($data,$user_id){
        $txnid = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
        $player_ids = isset($data['player_id']) ? substr($data['player_id'], 1) : '';
        $amount_per_user = isset($data['amount_per_user']) ? substr($data['amount_per_user'], 1) : '';
        $order_code = isset($data['order_code']) ? substr($data['order_code'], 1) : '';
        $player_order_id = isset($data['player_order_id']) ? substr($data['player_order_id'], 1) : '';
        $playercode = isset($data['playercode']) ? substr($data['playercode'], 1) : '';
        $InsertData = array_filter(array(
            "user_id" => $user_id,
            "amount" => $data['amount'],
            "player_id" => $player_ids,
            'playercode' => $playercode,
            'amount_per_user' => $amount_per_user,
            'player_order_id' => $player_order_id,
            'order_code' => $order_code,
            'txnid' => $txnid,
            'created_at' => date('Y-m-d H:i:s')
        ));
        $this->db->insert('player_cashout_temp', $InsertData);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        // $ID = $this->db->insert_id();
        return $InsertData['txnid'];
    }

    function saveTransactionDetails($data){
        $transfer_data = $this->getPlayerCheckoutTransfer('id,bank_ref_num,payuMoneyId', array('txnid'=>$data['txnid']));
        if(!$transfer_data){
            $player_temp_data = $this->getPlayerTempDetails('amount_per_user,player_order_id,playercode,player_id,user_id,order_code',array('txnid'=>$data['txnid']));
            $InsertData = array_filter(array(
                "user_id" => isset($player_temp_data[0]['user_id']) ? $player_temp_data[0]['user_id'] : 0,
                "amount" => $data['amount'],
                'txnid' => $data['txnid'],
                'payu_mihpayid' => $data['mihpayid'],
                'status' => $data['status'],
                'payu_paymentId' => $data['paymentId'],
                'bank_ref_num' => $data['bank_ref_num'],
                'payuMoneyId' => $data['payuMoneyId'],
                'addedon' => $data['addedon'],
                'mode' => $data['mode'],
                'created_at' => date('Y-m-d H:i:s')
            ));
            $this->db->insert('payumoney_cashout_transfer', $InsertData);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            }
            $id = $this->db->insert_id();

            $result = $this->addPlayerDetails($data,$InsertData['user_id'],$id,$player_temp_data[0]);
            if($result){
                // $request_array = [];
                // foreach($result as $players){
                //     $merchantRefId = $players['id'];
                //     $player = $this->updatePlayerDetails(array("merchantRefId" => $merchantRefId),$players['id']);
                //     $player_detail = $this->getPlayerDetails($players);
                //     $player_array = [];
                //     if($player_detail['bankAccountNo']){
                //         $player = $this->updatePlayerDetails(array("name" => isset($player_detail['name']) ? $player_detail['name'] : '', 'email' => isset($player_detail['email']) ? $player_detail['email'] : '', 'mobile_number' => isset($player_detail['mobile']) ? $player_detail['mobile'] : '', 'beneficiaryAccountNumber' => isset($player_detail['bankAccountNo']) ? $player_detail['bankAccountNo'] : '', 'beneficiaryIfscCode' => isset($player_detail['bankIFSC']) ? $player_detail['bankIFSC'] : ''),$players['id']);
                //         $player_array = [
                //             'beneficiaryAccountNumber' => isset($player_detail['bankAccountNo']) ? $player_detail['bankAccountNo'] : '',
                //             'beneficiaryIfscCode' => isset($player_detail['bankIFSC']) ? $player_detail['bankIFSC'] : '',
                //             'purpose' => 'transfer request',
                //             'amount' => isset($players['amount']) ? $players['amount'] : 0.00,
                //             'batchId' => '',
                //             'merchantRefId' => $merchantRefId,
                //             'paymentType' => 'IMPS',
                //             'id' => $players['id']
                //         ];
                //     }
                //     if($player_array){
                //         $request_array[] = $player_array;
                //     }
                // }
                // $request_array = array_chunk($request_array, 1000);
                // $batchId = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
                // foreach($request_array as $key => $request){
                //     foreach($request as $key_index => $player_array){
                //         $request_array[$key][$key_index]['batchId'] = $batchId;
                //         $player = $this->updatePlayerDetails(array("batchId" => $batchId),$player_array['id']);
                //         unset($request_array[$key][$key_index]['id']);
                //     }
                //     $batchId = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
                // }
                // print_r($request_array);
                // $result_token = $this->generateAccessToken();
                // if($result_token == FALSE){
                //     return FALSE;
                // }
                // $access_token = $result_token['access_token'];
                // $refresh_token = $result_token['refresh_token'];
                // foreach ($request_array as $request) {
                //     $request_json = json_encode($request);
                //     $result = $this->transferApiRequest($request_json, $access_token);
                //     $result_token = $this->generateRefreshToken($refresh_token);
                //     if($result_token == FALSE){
                //         return FALSE;
                //     }
                //     $access_token = $result_token['access_token'];
                //     $refresh_token = $result_token['refresh_token'];
                // }
                return TRUE;
            }
            return FALSE;
        }
        return NULL;
    }

    function transferApiRequest($request_json, $access_token){
        $ApiUrl = PAYUMONEY_URL.'/payment';
        /* Excecute Curl */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_json);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' .$access_token;
        $headers[] = 'payoutMerchantId: '. PAYOUT_MERCHANTID;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $this->addLogData($request_json,$ApiUrl,$result);
        $result = json_decode($result, TRUE);
        if (curl_errno($ch)) {
            return FALSE;
        }
        curl_close($ch);
        return $result;
    }

    function addLogData($requestData, $url, $response){
        $InsertData = array_filter(array(
            'url' => $url,
            'request_data' => $requestData,
            'response' => $response,
            'created_at' => date('Y-m-d H:i:s')
        ));
        $this->db->insert('log_data', $InsertData);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
    }

    function addPlayerDetails($data,$user_id,$id,$playerData){
        if(isset($playerData['player_id'])){
             $user_id_array = explode(',', $playerData['player_id']);
        }
        if(isset($playerData['amount_per_user'])){
            $amount = explode(',', $playerData['amount_per_user']);
        }
        if(isset($playerData['order_code'])){
            $order_code_array = explode(',', $playerData['order_code']);
        }
        if(isset($playerData['playercode'])){
            $playercode_array = explode(',', $playerData['playercode']);
        }
        if(isset($playerData['player_order_id'])){
            $player_order_id_array = explode(',', $playerData['player_order_id']);
        }
        $dataInserted = [];
        foreach($user_id_array as $key => $player_id){
            $InsertData = array_filter(array(
                "user_id" => $user_id,
                "cashout_transfer_id" => $id,
                'txnid' => $data['txnid'],
                'status' => "PENDING",
                'player_id' => $player_id,
                'order_code' => isset($order_code_array[$key]) ? $order_code_array[$key] : '',
                'amount' => isset($amount[$key]) ? $amount[$key] : 0.00,
                'player_order_id' => isset($player_order_id_array[$key]) ? $player_order_id_array[$key] : '',
                'playercode' => isset($playercode_array[$key]) ? $playercode_array[$key] : '',
                'created_at' => date('Y-m-d H:i:s')
            ));
            $this->db->insert('player_cashout_detail', $InsertData);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            }
            $insertedID = $this->db->insert_id();
            $InsertData['id'] = $insertedID;
            $dataInserted[] = $InsertData;
        }
        return $dataInserted;
    }

    function generateAccessToken(){
        try{
            $client_id = PAYUMONEY_CLIENTID;
            // $client_secret = PAYUMONEY_CLIENTSECRET;
            $username = PAYUMONEY_USERID;
            $password = PAYUMONEY_PASSWORD;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, PAYUMONEY_AUTHURL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$username."&password=".$password."&scope=create_payout_transactions&grant_type=password&client_id=".$client_id);

            $headers = array();
            $headers[] = 'Cache-Control: no-cache';
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            $requestData = "username=".$username."&password=".$password."&scope=create_payout_transactions&grant_type=password&client_id=".$client_id;
            $this->addLogData($requestData,PAYUMONEY_AUTHURL,$result);
            if (curl_errno($ch)) {
                return FALSE;
            }
            curl_close($ch);
            $result = json_decode($result, TRUE);
            if(isset($result['access_token']) && isset($result['refresh_token'])){
                return ['access_token'=>$result['access_token'], 'refresh_token' => $result['refresh_token']];
            }
        } catch(Exception $e){
            return FALSE;
        }
    }

    function generateRefreshToken($refresh_token){
        try{
            $client_id = PAYUMONEY_CLIENTID;
            $client_secret = PAYUMONEY_CLIENTSECRET;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, PAYUMONEY_AUTHURL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=".$client_id."&grant_type=refresh_token&refresh_token=".$refresh_token);

            $headers = array();
            $headers[] = 'Cache-Control: no-cache';
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            $requestData = "client_id=".$client_id."&grant_type=refresh_token&refresh_token=".$refresh_token;
            $this->addLogData($requestData,PAYUMONEY_AUTHURL,$result);
            if (curl_errno($ch)) {
                return FALSE;
            }
            $result = json_decode($result,TRUE);
            if(isset($result['access_token']) && isset($result['refresh_token'])){
                return ['access_token'=>$result['access_token'], 'refresh_token' => $result['refresh_token']];
            }
            curl_close($ch);
        } catch(Exception $e){
            return FALSE;
        }
    }

    function getPlayerDetails($player){
        $player_id = $player['playercode'];
        $ApiUrl = GAME_API_URL_CRIC91.'/player/kyc/details/'.$player_id;
       // $ApiUrl = 'http://13.234.19.187:3500/player/kyc/details/'.$player_id;

        /* Excecute Curl */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer ' .AUTH_TOKEN_PLAYERCASHOUT;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $result = curl_exec($ch);
        $this->addLogData('',$ApiUrl,$result);
        $result = json_decode($result, TRUE);
        
        if (curl_errno($ch)) {
            return FALSE;
        }
        curl_close($ch);
        if($result['status'] == 'Approved'){
            return $result;
        } else{
            return FALSE;
        }
    }

    function updatePlayerDetails($data_array,$id){
        $this->db->where('id', $id);
        $this->db->limit(1);
        $this->db->update('player_cashout_detail', $data_array);
        return TRUE;
    }

    function updatePlayerDetailsRefId($data_array,$merchantRefId){
        $this->db->where('merchantRefId', $merchantRefId);
        $this->db->limit(1);
        $this->db->update('player_cashout_detail', $data_array);
        return TRUE;
    }

    function getTransaferDetail($Field,$Where,$multiRecords = FALSE){
        if($Field){
            $Params = array_map('trim', explode(',', $Field));
            $Field = '';
            $FieldArray = array(
                'user_id' => 'pcd.user_id',
                'cashout_transfer_id' => 'pcd.cashout_transfer_id',
                'txnid' => 'pcd.txnid',
                'player_id' => 'pcd.player_id',
                'amount' => 'pcd.amount',
                'batchId' => 'pcd.batchId',
                'merchantRefId' => 'pcd.merchantRefId',
                'id' => 'pcd.id',
                'player_order_id' => 'pcd.player_order_id',
                'email' => 'pcd.email',
                'name' => 'pcd.name',
                'mobile_number' => 'pcd.mobile_number',
                'bankReferenceId' => 'btd.bankReferenceId',
                'payuRefId' => 'btd.payuRefId',
                'playercode' => 'pcd.playercode',
                'status' => 'pcd.status',
                'order_code' => 'pcd.order_code'
            );
            if ($Params) {
                foreach ($Params as $Param) {
                    $Field .= (!empty($FieldArray[$Param]) ? ',' . $FieldArray[$Param] : '');
                }
            }
            if (!empty($Field)) $this->db->select($Field, FALSE);
        }
        $this->db->from('player_cashout_detail pcd');
        $this->db->join('bank_transaction_details btd', 'pcd.merchantRefId=btd.cashout_merchantReferenceId', 'left');

        if (!empty($Where['user_id'])) {
            $this->db->where("pcd.user_id", $Where['user_id']);
        }

        if (!empty($Where['cashout_transfer_id'])) {
            $this->db->where("pcd.cashout_transfer_id", $Where['cashout_transfer_id']);
        }

        if (!empty($Where['txnid'])) {
            $this->db->where("pcd.txnid", $Where['txnid']);
        }

        if (!empty($Where['status'])) {
            $this->db->where("pcd.status", $Where['status']);
        }

        if (!empty($Where['player_id'])) {
            $this->db->where("pcd.player_id", $Where['player_id']);
        }

        if (!empty($Where['merchantRefId'])) {
            $this->db->where("pcd.merchantRefId", $Where['merchantRefId']);
        }

        if (!empty($Where['batchId'])) {
            $this->db->where("pcd.batchId", $Where['batchId']);
        }

        if (!empty($Where['bankReferenceId'])) {
            $this->db->where("btd.bankReferenceId", $Where['bankReferenceId']);
        }
        $this->db->order_by('pcd.id', 'DESC');
        if($multiRecords){
            $TempOBJ = clone $this->db;
            $TempQ = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            // if ($PageNo != 0) {
            //     $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /* for pagination */
            // }
        } else{
            $this->db->limit(1);
        }

        $Query = $this->db->get();

        if ($Query->num_rows() > 0) {
            if ($multiRecords) {
                $Records = array();
                foreach ($Query->result_array() as $key => $Record) {
                    $Records[] = $Record;
                }
                $Return['Data']['Records'] = $Records;
            } else {
                $Record = $Query->row_array();
                return $Record;
            }
        } else{
            return [];
        }
        $Return['Data']['Records'] = empty($Records) ? array() : $Records;
        return $Return;
    }

    function updatePlayerCashoutStatus($orderId,$refrenceId){
        $ApiUrl = GAME_API_URL_CRIC91.'/player/cashout/order/approve';
        //$ApiUrl = 'http://13.234.19.187:3500/admin/order/player/approve';
        /* Excecute Curl */
        $request_json = [
            'orderId' => $orderId,
            'refrenceId' => $refrenceId
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_json));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' .AUTH_TOKEN_PLAYERCASHOUT;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $this->addLogData(json_encode($request_json),$ApiUrl,$result);
        $result = json_decode($result, TRUE);
        
        if (curl_errno($ch)) {
            return FALSE;
        }
        curl_close($ch);
        if($result['orderStatus'] == 'Approved' && $result['order_id'] == $order_id){
            return TRUE;
        } else{
            return FALSE;
        }
    }

    public function insertBankTransactionDetails($data){
        $this->db->insert('bank_transaction_details', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        return TRUE;
    }

    public function addPlyerDetailsFailed($data){
        $this->db->insert('player_cashout_detail', $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        return TRUE;
    }

    public function getPlayerTempDetails($Field,$Where,$multiRecords = FALSE){
        $Params = array_map('trim', explode(',', $Field));
        $FieldArray = array_filter(array(
            "user_id" => 'user_id',
            "amount" => 'amount',
            "player_id" => 'player_id',
            'playercode' => 'playercode',
            'amount_per_user' => 'amount_per_user',
            'player_order_id' => 'player_order_id',
            'order_code' => 'order_code'
        ));

        if ($Params) {
            foreach ($Params as $Param) {
                $Field .= (!empty($FieldArray[$Param]) ? ',' . $FieldArray[$Param] : '');
            }
        }

        if (!empty($Field)) $this->db->select($Field, FALSE);
        $this->db->from('player_cashout_temp');

        if (!empty($Where['id'])) {
            $this->db->where("id", $Where['id']);
        }
        if (!empty($Where['txnid'])) {
            $this->db->where("txnid", $Where['txnid']);
        }

        if($multiRecords){
            // $TempOBJ = clone $this->db;
            // $TempQ = $TempOBJ->get();
            // $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            // if ($PageNo != 0) {
            //     $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /* for pagination */
            // }
        } else{
            $this->db->limit(1);
        }

        $Query = $this->db->get();

        if ($Query->num_rows() > 0) {
            $data = $Query->result_array();
            return $data;
        } else{
            return [];
        }

    }

    public function getPlayerCheckoutTransfer($Field,$Where){
        $Params = array_map('trim', explode(',', $Field));
        $FieldArray = array_filter(array(
            'id' => 'id',
            "user_id" => 'user_id',
            "amount" => 'amount',
            "status" => 'status',
            'bank_ref_num' => 'bank_ref_num',
            'payuMoneyId' => 'payuMoneyId',
            'mode' => 'mode'
        ));

        if ($Params) {
            foreach ($Params as $Param) {
                $Field .= (!empty($FieldArray[$Param]) ? ',' . $FieldArray[$Param] : '');
            }
        }
        if (!empty($Field)) $this->db->select($Field, FALSE);
        $this->db->from('payumoney_cashout_transfer');

        if (!empty($Where['id'])) {
            $this->db->where("id", $Where['id']);
        }
        if (!empty($Where['txnid'])) {
            $this->db->where("txnid", $Where['txnid']);
        }
        $this->db->limit(1);
        $Query = $this->db->get();
        $Record = $Query->row_array();
        return $Record;
    }

}
