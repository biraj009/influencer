<?php include('header.php');?>

  <section class="bg-img pt-10 pb-5" style="background-image: url('assets/img/bg.jpg');">
    <div class="container">
      <div class="aboutUs">
        <div class="top-header-title text-center">
          <h3 class="mb-0"> Refund Policy </h3>
        </div>
        <div class="row">
          <div class="about_us_content py-3 px-5">
            <div class="col-sm-12">
              <p> At Battlefight we believe "Your money is yours & you should get it immediately when you want". We are working with the best banking & gateway partners to payout our users at the earliest possible. </p>
              
              <p> However, all the refunds at Battlefight are subject to following conditions :</p>


              <blockquote type="disc">
                  <li>All the refunds are processed to the same mode / financial instruments through which the deposits were made.</li>
                  <li class="my-2">In some cases, internal review team may take upto 24 working hours to process a withdrawal.</li>
                 
                  <li class="my-2">Immediate withdrawal after deposits, without playing matches will invite transaction fee charges, as applicable.</li>

                  <li>All the winnings above Rs 10,000 is subject to applicable TDS deduction.</li>

                  <li class="my-2">Battlefight  reserves the right to modify/change these terms &amp; conditions anytime without any notice.</li>
              </blockquote>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include('footer.php');?>
