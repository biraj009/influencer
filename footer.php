</main>
<!-- footer start -->
<footer class="pb-3 site-footer bg_secondary">
  <div class="container">
    <div class="row pt-5 border-bottom border-dark">
      <div class="col-lg-6 col-md-7">
        <div class="row">
          <div class="col-sm-4">
            <h6>quick links</h6>
            <ul class="footer_menu list-unstyled">
                <li><a href="about-us.php"> About Us </a></li>
                <li><a href="contact-us.php"> Contact Us </a></li>
                <li><a href="howtoplay.php"> How to Play </a></li>
            </ul>
          </div>
          <div class="col-sm-4">
            <h6>Support</h6>
            <ul class="footer_menu list-unstyled">

              <li><a href="legality.php"> Legality </a></li>
              <li><a href="faq.php"> FAQs </a></li>
              <li><a href="refundpolicy.php"> Refund Policy </a></li>
              <li><a href="disclaimer.php"> Disclaimer </a></li>

            </ul>
          </div>
          <div class="col-sm-4">
            <h6>News & Resources</h6>
            <ul class="footer_menu list-unstyled">
              
              <li><a href="javascript:;"> Fantasy Blog </a></li>
              <li><a href="point-system.php"> Fantasy Points System </a></li>
              <li><a href="privacy-policy.php"> Privacy Policy </a></li>
              <li><a href="term-conditions.php"> Term & Conditions </a></li>

            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-4 offset-lg-2 col-md-5 socialIcons">
        <h6>Stay in touch with us</h6>
        <div class="social_menu">
            <a class="fb" href="javascript:;"><i class="fab fa-facebook-f"></i></a>
            <a class="tw" href="javascript:;"><i class="fab fa-twitter"></i></a>
            <a class="gplus" href="javascript:;"><i class="fab fa-google-plus-g"></i></a>
            <a class="inst" href="javascript:;"><i class="fab fa-instagram"></i></a>
          </div>
      </div>
    </div>
    <div class="copyright text-center text-white pt-3">
      <p class="mb-1">Copyright © 2020.  All rights reserved.</p>
<!--       <img src="assets/img/logo.png" alt="logo" class="img-fluid " width="90px">
 -->    </div>
  </div>
</footer>
<!-- footer End -->

<!--- Loader Start-->
  <div loading class="loader_wrapr" id="loderBG">
     <div class="loader-ripple"><div></div><div></div></div>
  </div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/custom.js"></script>

<script language="javascript" type="text/javascript">
    $(window).on('load', function(){
      setTimeout(removeLoader, 500); //wait for page load PLUS two seconds.
    });
    function removeLoader(){
        $( "#loderBG" ).fadeOut(500, function() {
          // fadeOut complete. Remove the loading div
          $( "#loderBG" ).remove(); //makes page more lightweight 
      });  
    }
</script>






