<style type="text/css">
.cards-list {
  z-index: 0;
  

  justify-content: space-around;
  flex-wrap: wrap;
}

.card {
  margin: 30px auto;
  width: 320px;
  height: 151px;
  border-radius: 20px;
//box-shadow: 5px 5px 30px 7px rgba(0,0,0,0.25), -5px -5px 30px 7px rgba(0,0,0,0.22);
  cursor: pointer;
  transition: 0.4s;
  background-color: #37475e;
}

.card .card_image {
  width: 300px;
  height: 150px;
  border-radius: 20px;
}

.card .card_image img {
  width: 150px;
  height: 150px;
  border-radius: 20px;
  object-fit: cover;
}

.card .card_title {
  text-align: right;
  border-radius: 0px 0px 40px 40px;
  font-family: sans-serif;
  font-weight: bold;
  font-size: 20px;
  margin-top: -80px;
  color: #fff;
  margin-right: 15px;
  height: 40px;
}

/*.card:hover {
  transform: scale(0.9, 0.9);
  box-shadow: 5px 5px 30px 15px rgba(0,0,0,0.25), 
    -5px -5px 30px 15px rgba(0,0,0,0.22);
}*/

.title-white {
  color: white;
}

.title-black {
  color: black;
}

@media all and (max-width: 500px) {
  .card-list {
    /* On small screens, we are no longer using row direction but column */
    flex-direction: column;
  }
}

</style>



<div class="panel-body" ng-controller="PageController" ng-init="getList()"><!-- Body -->
  <div class="row">
    <div class="col-lg-4 ">
 <a href="<?php echo base_url().'playerCashOut'?>"> 
<div class="col-xl-4 col-sm-6 mb-3">
<div class="cards-list">
<div class="card 4">
  <div class="card_image">
    <img src="https://media.giphy.com/media/LwIyvaNcnzsD6/giphy.gif" />
    </div><br>
  <div class="card_title title-black">
    <p>Player Cashout</p>
  </div>
  
  </div>

</div>
</div></a>
</div>

<!-- Body -->
  <div class="col-lg-4">
 <a href="<?php echo base_url().'transferDetails'?>"> 
<div class="col-xl-4 col-sm-6 mb-3">
<div class="cards-list">
<div class="card 4">
  <div class="card_image">
    <img src="https://media.giphy.com/media/LwIyvaNcnzsD6/giphy.gif" />
    </div><br>
  <div class="card_title title-black">
    <p>Transfer Details</p>
  </div>
  
  </div>

</div>
</div></a>
</div>
</div>
</div>
<!-- Body/ -->
