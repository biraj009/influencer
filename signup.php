<?php include('header.php'); ?>
<section class="login_signup_wrapr burger ">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-10 offset-md-1 offset-lg-0">
				<form action="" class="site_form signup">
					<h1>Sign up</h1>
					<div class="d-flex">
						<div class="col-md-6 pl-0 form-group">
							<img src="assets/img/user.svg" alt="user" class="img-fluid">
							<input type="text" placeholder=" Full Name" >
						</div>
						<div class="col-md-6 pl-0 form-group">
							<img src="assets/img/user.svg" alt="user" class="img-fluid">
							<input type="text" placeholder=" User Name" >
						</div>
					</div>
					<div class="d-flex">
						<div class="col-md-6 pl-0 form-group">
							<img src="assets/img/contact.png" alt="user" class="img-fluid">
							<input type="text" placeholder="Mobile Number" >
						</div>
						<div class="col-md-6 pl-0 form-group">
							<img src="assets/img/email.png" alt="user" class="img-fluid">
							<input type="text" placeholder="Email" >
						</div>
					</div>
					<div class="d-flex">
						<div class="col-md-6 pl-0 form-group">
							<img src="assets/img/email.png" alt="user" class="img-fluid">
							<input type="text" placeholder="Confirm Email" >
						</div>
						<div class="col-md-6 pl-0 form-group">
							<img src="assets/img/padlock.svg" alt="user" class="img-fluid">
							<input type="text" placeholder="Passwrod" >
						</div>
					</div>
					<div class="col-md-6 pl-0 form-group">
						<img src="assets/img/padlock.svg" alt="user" class="img-fluid">
						<input type="text" placeholder="Confirm Passwrod" >
					</div>
					<div class="custom-control custom-checkbox">
					  <input type="checkbox" class="custom-control-input" id="customCheck1">
					  <label class="custom-control-label" for="customCheck1">I Accept all <a href="javascript:;"  class="text-white">Terms & Conditions</a></label>
					</div>
					<div class="col-md-3 pl-0">
						<button type="submit" value="sign up" class="btn_primary my-3  w-100">Sign Up</button>
					</div>
					<div class="">Already have an account? <a href="login.php" class="text-white text_underline">Sign in</a></div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>    
