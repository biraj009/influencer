<?php include('header.php');?>

  <section class="bg-img pt-10 pb-5" style="background-image: url('assets/img/bg.jpg');">
    <div class="container">
      <div class="aboutUs">
        <div class="top-header-title text-center">
          <h3 class="mb-0"> Disclaimer </h3>
        </div>
        <div class="row">
          <div class="about_us_content py-3 px-5">
            <div class="col-sm-12">
              <p> Battlefight is not affiliated in any way to and claims no association, Battlefight acknowledges that the ICC, BCCI, IPL and its franchises/teams. Own all proprietary names and marks relating to the relevant tournament or competition. Residents of the states of Assam, Odisha and Telangana, and where otherwise prohibited by law are not eligible to enter Battlefight leagues. </p>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include('footer.php');?>
