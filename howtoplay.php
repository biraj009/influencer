<?php include('header.php');?>

  <section class="bg-img pt-10 pb-5" style="background-image: url('assets/img/bg.jpg');">
    <div class="container">
      <div class="aboutUs">
        <div class="top-header-title text-center">
          <h3 class="mb-0"> How to Play </h3>
        </div>
        <div class="row">
          <div class="about_us_content py-3 px-5">
            <div class="col-md-12">
                <p> <strong> Select a Matches :</strong> Select a Matches from any of the current or upcoming Cricket Matches. </p>
            </div>

            <div class="row py-3 align-items-stretch">
                <div class="col-lg-3 col-sm-6 text-center">
                    <div class="howtoplay-item">
                        <span> <img src="assets/img/step1.svg" alt=""></span>
                        <h3> Step 1 </h3>
                        <p> Choose an upcoming match according to your choice available on the website. </p>
                    </div>    
                </div>
                <div class="col-lg-3 col-sm-6 mt-3 mt-sm-0 text-center">
                    <div class="howtoplay-item">
                        <span> <img src="assets/img/step2.svg" alt=""> </span>
                        <h3> Step 2 </h3>
                        <p> Choose the group of your favorite players and create your ideal team. </p>
                    </div>    
                </div>
                <div class="col-lg-3 col-sm-6 mt-3 mt-lg-0 text-center">
                    <div class="howtoplay-item">
                        <span> <img src="assets/img/step3.svg" alt=""> </span> 
                        <h3> Step 3 </h3>
                        <p> Peg your team against others in an existing contest or create a contest of your own.</p>
                    </div>    
                </div>
                <div class="col-lg-3 col-sm-6 mt-3 mt-lg-0 text-center">
                    <div class="howtoplay-item">
                        <span> <img src="assets/img/step4.svg" alt=""> </span>
                        <h3> Step 4 </h3>
                        <p> Check out your score in real time and win the prizes for highest scoring line up. </p>
                    </div>    
                </div>
            </div>

            <div class="col-md-12">
                <h3> Create your Team </h3>
                <p> Use you cricketing knowledge and showcase your team management skills to create your Battlefight team in various league formats. </p>


                <p> <strong> Classic Battlefight </strong></p>
                
                <div class="row classic_list justify-content-stretch">
                    
                    <div class="col-md-6">
                        <ol>
                            <li>Select a total of 11 players from the two teams.</li>
                            <li>Maximum 7 Players from One team.</li>
                            <li>Select your captain &amp; Vice-Captain.</li>
                            <li>Maximum credits allotted: 100</li>
                        </ol>
                    </div>

                    <div class="col-md-6">
                      <div class="bg-light p-4 h-100">
                           <h4> Note : </h4>
                              <p> <strong> Captain </strong> – Get 2x points scored by the Individual. </p>
                              <p><strong> Vice-Captain </strong> – Get 1.5x points scored by the individual. </p>
                      </div>    
                    </div>
                </div>    


                <p> <strong> Batting Battlefight </strong></p>
                
                <div class="row classic_list justify-content-stretch">
                    
                    <div class="col-md-6">
                        <ol>
                            <li> Select a total of 5 players from 2 teams who will score maximum runs according to you. </li>
                            <li>Maximum 3 Players from 1 team. </li>
                            <li> Select your captain & Vice-Captain. </li>
                            <li> Maximum credits allotted: 45 </li>
                        </ol>
                    </div>

                    <div class="col-md-6">
                      <div class="bg-light p-4 h-100">
                           <h4> Note : </h4>
                              <p> <strong> Captain </strong> – Get 2x points scored by the Individual. </p>
                              <p><strong> Vice-Captain </strong> – Get 1.5x points scored by the individual. </p>
                      </div>    
                    </div>
                </div>    
                
                <p> <strong> Bowling Battlefight </strong></p>
                
                <div class="row classic_list justify-content-stretch">
                    
                    <div class="col-md-6">
                        <ol>
                            <li>Select a total of 5 players from 2 teams who will the maximum wickets according to you.</li>
                            <li>Maximum 3 Players from 1 team.</li>
                            <li>Select your captain & Vice-Captain.</li>
                            <li>Maximum credits allotted: 45</li>
                        </ol>
                    </div>

                    <div class="col-md-6">
                      <div class="bg-light p-4 h-100">
                           <h4> Note : </h4>
                              <p> <strong> Captain </strong> – Get 2x points scored by the Individual. </p>
                              <p><strong> Vice-Captain </strong> – Get 1.5x points scored by the individual. </p>
                      </div>    
                    </div>
                </div>    
                
                 <p> <strong> Join a contest </strong></p>
                  <div class="ml-3">
                      <p> <strong> Cash  </strong> –  Chance to win real cash. </p>
                      <p><strong> Practice contest </strong> – Win ultimate bragging rights to show off your improvement and move to real cash games. </p>
                  </div>

                  <p> <strong> Follow the match </strong></p>
                  <p> Watch the live match and track your Battlefight scorecard.</p>

                  <p> <strong> Withdraw winnings </strong></p>
                  <p> Withdraw your winnings conveniently to your Bank.</p>

            </div>

          </div>
        </div>
      </div>
    </div>
  </section>

<?php include('footer.php');?>
