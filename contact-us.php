<?php include('header.php');?>

  <section class="bg-img playerStatus pt-10 pb-5" style="background-image: url('assets/img/bg.jpg');">
    <div class="container">
      <div class="contactUs">
        <div class="top-header-title text-center">
          <h3 class="mb-0"> Contact Us </h3>
        </div>
        <div class="contactBox p-5">
          <div class="row">
            <div class="col-md-6 col-sm-12 order-2 order-md-1">
              <div class="contectInformation mt-2">
                <ul>
                  <li><i class="fa fa-home"></i> <span> ear by abcd front of xyz Battlefight Pvt. Ltd. </span></li>
                  <li><i class="fa fa-phone"></i> <span>+91-1254678921 </span></li>
                  <li><i class="fa fa-envelope"></i> <span>info@Battlefight.in </span></li>
                  <li><i class="fa fa-map-marker pr-1"></i> <span>xyz, new plasia, Indore Madhya Pradesh, India, 452001 </span></li>
                </ul>
              </div>
              <div class="listPar ml-2">
                <p>Let's Connect</p>
                <div class="social_menu">
                  <a href="http://facebook.com" class="facebook"><i class="fab fa-facebook-f"></i></a>
                  <a href="https://twitter.com/" class="twitter"><i class="fab fa-twitter"></i></a>
                  <a href="javascript:;" class="google"><i class="fab fa-google-plus-g"></i></a>
                  <a href="https://www.instagram.com/accounts/login/" class="insta"><i class="fab fa-instagram"></i></a>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 order-1 order--md-2">
              <form class="form_commen ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-valid-email" name="contactUsForm" ng-submit="contactUS(contactUsForm)" novalidate="">
                <div class="contactField">
                  <div class="form-group">
                    <input type="text" class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Name" ng-model="contactForm.Name" name="name" ng-required="true" required="required">
                    <div style="color:red" ng-show="submitted &amp;&amp; contactUsForm.name.$error.required" class="form-error ng-hide">
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required ng-valid-pattern ng-valid-email" placeholder="Email" ng-model="contactForm.Email" name="email" ng-required="true" ng-change="removeMassage()" ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" required="required">
                    <div style="color:red" ng-show="submitted &amp;&amp; contactUsForm.email.$error.required" class="form-error ng-hide">
                    </div>
                    <div style="color:red" ng-show="contactUsForm.email.$error.pattern" class="form-error ng-hide">
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Phone" ng-model="contactForm.PhoneNumber" name="phoneNumber" ng-required="true" required="required">
                    <div style="color:red" ng-show="submitted &amp;&amp; contactUsForm.phoneNumber.$error.required" class="form-error ng-hide">
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Subject" ng-model="contactForm.Title" name="subject" ng-required="true" required="required">
                    <div style="color:red" ng-show="submitted &amp;&amp; contactUsForm.subject.$error.required" class="form-error ng-hide">
                    </div>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control ng-pristine ng-untouched ng-empty ng-invalid ng-invalid-required" placeholder="Message" ng-model="contactForm.Message" name="message" ng-required="true" required="required"></textarea>
                    <div style="color:red" ng-show="submitted &amp;&amp; contactUsForm.message.$error.required" class="form-error ng-hide">
                    </div>
                  </div>
                  <div class="form-group">
                    <!-- ngIf: errorStatus==500 -->
                  </div>
                  <div class="form-group">
                    <button class="btn btn-submit">SEND MESSAGE</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include('footer.php');?>