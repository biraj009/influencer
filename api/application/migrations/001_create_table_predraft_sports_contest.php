<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Table_predraft_sports_contest extends CI_Migration {

        public function up()
        {
                $fields = array(
                          "`PredraftContestID` int AUTO_INCREMENT NOT NULL ",
                          "`ContestID` int NOT NULL",
                          "`ContestGUID` varchar(36) NOT NULL",
                          "`UserID` int NOT NULL",
                          "`RoomID` varchar(20) DEFAULT NULL",
                          "`RoomPassword` varchar(50) DEFAULT NULL",
                          "`CategoryID` int DEFAULT NULL",
                          "`TotalQuestion` smallint DEFAULT NULL",
                          "`Map` enum('Erangel','Miramar','Vikendi','Sanhok','TDM') DEFAULT NULL",
                          "`Platform` varchar(100) DEFAULT NULL",
                          "`GameVersion` varchar(30) DEFAULT NULL",
                          "`WinningType` enum('PerKill','PrizePool','Both') DEFAULT 'PrizePool'",
                          "`MainTopBannerImage` varchar(255) DEFAULT NULL",
                          "`IconImage` varchar(255) DEFAULT NULL",
                          "`GameRule` varchar(255) DEFAULT NULL",
                          "`PerKill` int DEFAULT NULL",
                          "`GameSchedule` date DEFAULT NULL",
                          "`FrequencyType` varchar(30) DEFAULT NULL",
                          "`EndDate` date DEFAULT NULL",
                          "`GameScheduleTime` varchar(10) DEFAULT NULL",
                          "`PreContestID` int DEFAULT NULL",
                          "`LeagueType` enum('Safe') DEFAULT NULL",
                          "`GameType` enum('Solo','Duo','Squad') DEFAULT NULL",
                          "`GameTimeLive` smallint DEFAULT '0'",
                          "`LeagueJoinDateTime` datetime DEFAULT NULL",
                          "`GameEndDateTime` datetime DEFAULT NULL",
                          "`AdminPercent` smallint DEFAULT NULL",
                          "`ContestFormat` enum('Head to Head','League') NOT NULL DEFAULT 'League'",
                          "`ContestType` enum('Normal','Reverse','InPlay','Hot','Champion','Practice','More','Mega','Winner Takes All','Only For Beginners','Head to Head','Infinity Pool','Smart Pool') NOT NULL",
                          "`IsAutoCreate` enum('No','Yes') NOT NULL DEFAULT 'No'",
                          "`ContestName` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL",
                          "`Privacy` enum('Yes','No') DEFAULT NULL",
                          "`IsPaid` enum('Yes','No') NOT NULL",
                          "`IsConfirm` enum('Yes','No') NOT NULL DEFAULT 'No'",
                          "`WinningAmount` int NOT NULL DEFAULT '0'",
                          "`ContestSize` int NOT NULL",
                          "`TotalJoinedTeams` int NOT NULL DEFAULT '0'",
                          "`CashBonusContribution` float(6,2) NOT NULL DEFAULT '0.00'",
                          "`UserJoinLimit` int NOT NULL DEFAULT '1'",
                          "`EntryType` enum('Single','Multiple') NOT NULL",
                          "`EntryFee` int NOT NULL DEFAULT '0'",
                          "`NoOfWinners` int NOT NULL",
                          "`CustomizeWinning` text",
                          "`UserInvitationCode` varchar(50) DEFAULT NULL",
                          "`IsWinningAssigned` enum('No','Yes','Moved') NOT NULL DEFAULT 'No'",
                          "`IsWinningDistributed` enum('No','Yes') NOT NULL DEFAULT 'No'",
                          "`MinimumUserJoined` int DEFAULT NULL",
                          "`IsRefund` enum('Yes','No') NOT NULL DEFAULT 'No'",
                          "`IsWinningDistribute` enum('Yes','No') NOT NULL DEFAULT 'No'",
                          "`IsWinningDistributeAmount` enum('Yes','No') NOT NULL DEFAULT 'No'",
                          "`ContestTransferred` enum('Yes','No') NOT NULL DEFAULT 'No'",
                          "`isMailSent` enum('Yes','No') NOT NULL DEFAULT 'No'",
                          "`VideoURL` text",
                          "PRIMARY KEY (`PredraftContestID`)",
                );
                $attributes = array('ENGINE' => 'InnoDB');
                $this->dbforge->add_field($fields);
               // $this->dbforge->add_key('PredraftContestID', TRUE);
                $this->dbforge->create_table('predraft_sports_contest', TRUE, $attributes);
        }

        public function down()
        {
        }
}