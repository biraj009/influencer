<?php include('header.php'); ?>
<section class="login_signup_wrapr burger ">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-8 offset-md-2 offset-lg-0">
				<form action="" class="site_form login_form ">
					<h1>Login </h1>
					<div class="form-group">
						<img src="assets/img/user.svg" alt="user" class="img-fluid">
						<input type="text" placeholder="enter full name" >
					</div>
					<div class="form-group">
						<img src="assets/img/padlock.svg" alt="user" class="img-fluid">
						<input type="password"  placeholder="enter password" >
					</div>
					<div class="text-right"><a href="javascript:;" class="themeClr">Forget Password</a></div>
						<button type="submit"  class="mt-4 mb-3 w-100">Login</button>
						<div class="other_login mb-3">
							<a href="javascript:;"><img src="assets/img/fb.png" alt="fb">Facebook</a>
							<a href="javascript:;"><img src="assets/img/google.png" alt="fb">Google</a>
						</div>
					<div class="text-center">Don’t have an account? <a href="signup.php" class="text-white text_underline">Register</a></div>
				</form>
			</div>
		</div>
	</div>
</section>
<?php include('footer.php'); ?>    
