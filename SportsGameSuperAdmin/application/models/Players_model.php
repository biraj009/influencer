
<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Players_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->model('Utility_model');
    }


    function getPlayersList() {   
        $ApiUrl = GAME_API_URL_CRIC91.'/player/cashout/order?page=1&limit=100&type=Cash%20Out&orderStatus=Pending';

        /* Excecute Curl */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer ' .AUTH_TOKEN_PLAYERCASHOUT;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $result = curl_exec($ch);
        $result = json_decode($result, TRUE);
        
        if (curl_errno($ch)) {
            return FALSE;
        }
        curl_close($ch);

        return $result;

    
    }

}
