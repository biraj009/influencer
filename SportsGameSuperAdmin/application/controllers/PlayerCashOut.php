<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class PlayerCashOut extends Admin_Controller_Secure {
	
	/*------------------------------*/
	/*------------------------------*/	
	
	function __construct() {
        parent::__construct();
        $this->load->model('Players_model');
		
    }
	
	public function index($success = 0)
	{
	    //$this->load->model('Players_model');
		$data = array();
	    $postdata = $_POST;
        if (isset($postdata ['key'])) {
            $key                =   $postdata['key'];
            $salt               =   PAYUMONEY_SALT;
            $txnid              =   $postdata['txnid'];
            $amount             =   $postdata['amount'];
            $productInfo        =   $postdata['productinfo'];
            $firstname          =   $postdata['firstname'];
            $email              =   $postdata['email'];
            $udf5               =   $postdata['udf5'];
            $mihpayid           =   $postdata['mihpayid'];
            $status             =   $postdata['status'];
            $resphash           =   $postdata['hash'];
            //Calculate response hash to verify 
            $keyString          =   $key.'|'.$txnid.'|'.$amount.'|'.$productInfo.'|'.$firstname.'|'.$email.'|||||'.$udf5.'|||||';
            $keyArray           =   explode("|",$keyString);
            $reverseKeyArray    =   array_reverse($keyArray);
            $reverseKeyString   =   implode("|",$reverseKeyArray);
            $CalcHashString     =   strtolower(hash('sha512', $salt.'|'.$status.'|'.$reverseKeyString));
            if ($status == 'success'  && $resphash == $CalcHashString) {
            	$JSON = json_encode(array(
            		'postdata' => $postdata
		        ));
		        $Response = APICall(API_URL . 'admin/utility/saveTransactionDetails', $JSON); /* call API and get response */
		        if($Response['ResponseCode'] == 200 && $Response['Message'] == 'success'){
		        	$data['payUResopose'] = 'success';
		        } else if($Response['ResponseCode'] == 500 && $Response['Message'] == 'faliure'){
		        	$data['payUResopose'] = 'faliure';
		        }
            } else{
            	$data['payUResopose'] = 'faliure';
            }
        }
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css'
		);
		$load['js']=array(	
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',
		);	
		
		$result = $this->Players_model->getPlayersList();
		$JSON = json_encode(array(
            "select" => 'player_id,player_order_id,playercode',
            "where" => [],
            "multiRecords" => true,
            'SessionKey' => $this->SessionKey
        ));
        $Response = APICall(API_URL . 'admin/utility/getTransafer', $JSON); /* call API and get response */
        $Response = isset($Response['Data']['Data']) ? $Response['Data']['Data'] : $Response['Data'];
		if($result){
			foreach ($result[0]['data'] as $key => $value) {
				if($value['orderStatus'] == 'Pending'){
					$data['playerList'][] = $value;
						
				}	
			}
		}
		if(isset($Response['Records'])){
			foreach($data['playerList'] as $key => $playerInfo){
				foreach($Response['Records'] as $keyIndex => $PlayerDetail){
					if(isset($playerInfo['playerId']['_id'])){
						if($PlayerDetail['player_id'] == $playerInfo['playerId']['_id'] && $PlayerDetail['player_order_id'] == $playerInfo['_id']){
							unset($data['playerList'][$key]);
							continue;
						}
					}
				}
			}
		}
		$data['totalRecords'] = count($data['playerList']);
		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('playerCashOut/players_list',$data);
		$this->load->view('includes/footer');
	}

	public function getCallbackUrl(){
        $postdata = $_POST;
        if (isset($postdata ['key'])) {
            $key                =   $postdata['key'];
            $salt               =   PAYUMONEY_SALT;
            $txnid              =   $postdata['txnid'];
            $amount             =   $postdata['amount'];
            $productInfo        =   $postdata['productinfo'];
            $firstname          =   $postdata['firstname'];
            $email              =   $postdata['email'];
            $udf5               =   $postdata['udf5'];
            $mihpayid           =   $postdata['mihpayid'];
            $status             =   $postdata['status'];
            $resphash               =   $postdata['hash'];
            //Calculate response hash to verify 
            $keyString          =   $key.'|'.$txnid.'|'.$amount.'|'.$productInfo.'|'.$firstname.'|'.$email.'|||||'.$udf5.'|||||';
            $keyArray           =   explode("|",$keyString);
            $reverseKeyArray    =   array_reverse($keyArray);
            $reverseKeyString   =   implode("|",$reverseKeyArray);
            $CalcHashString     =   strtolower(hash('sha512', $salt.'|'.$status.'|'.$reverseKeyString));
            
            
            $data = array();
            if ($status == 'success'  && $resphash == $CalcHashString) {
            	$JSON = json_encode(array(
            		'postdata' => $postdata
		        ));
		        print_r($JSON);
		        die();
		        $Response = APICall(API_URL . 'admin/utility/saveTransactionDetails', $JSON); /* call API and get response */
		        if($Response['ResponseCode'] == 200 && $Response['Message'] == 'success'){
		        	$data['payUResopose'] = 'success';
		        } else if($Response['ResponseCode'] == 500 && $Response['Message'] == 'faliure'){
		        	$data['payUResopose'] = 'faliure';
		        }
            } else{
            	$data['payUResopose'] = 'faliure';
            }
            $load['css']=array(
			'asset/plugins/chosen/chosen.min.css'
			);
			$load['js']=array(	
				'asset/js/'.$this->ModuleData['ModuleName'].'.js',
				'asset/plugins/chosen/chosen.jquery.min.js',
				'asset/plugins/jquery.form.js',
			);	
			
			$result = $this->Players_model->getPlayersList();
			$JSON = json_encode(array(
	            "select" => 'player_id,player_order_id,playercode',
	            "where" => [],
	            "multiRecords" => true,
	            'SessionKey' => $this->SessionKey
	        ));
	        $Response = APICall(API_URL . 'admin/utility/getTransafer', $JSON); /* call API and get response */
	        $Response = isset($Response['Data']['Data']) ? $Response['Data']['Data'] : $Response['Data'];
			if($result){
				foreach ($result[0]['data'] as $key => $value) {
					if($value['orderStatus'] == 'Pending'){
						$data['playerList'][] = $value;
							
					}	
				}
			}
			if(isset($Response['Records'])){
				foreach($data['playerList'] as $key => $playerInfo){
					foreach($Response['Records'] as $keyIndex => $PlayerDetail){
						if(isset($playerInfo['playerId']['_id'])){
							if($PlayerDetail['player_id'] == $playerInfo['playerId']['_id'] && $PlayerDetail['player_order_id'] == $playerInfo['_id']){
								unset($data['playerList'][$key]);
								continue;
							}
						}
					}
				}
			}
			$data['totalRecords'] = count($data['playerList']);
			$this->load->view('includes/header',$load);
			$this->load->view('includes/menu');
			$this->load->view('playerCashOut/players_list',$data);
			$this->load->view('includes/footer');
        }
    }

}