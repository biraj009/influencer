<?php include('header.php');?>

  <section class="bg-img pt-10 pb-5" style="background-image: url('assets/img/bg.jpg');  background-attachment: fixed;">
    <div class="container">
      <div class="col-md-10 offset-md-1 pointSystem">
        <div class="top-header-title text-center">
          <h3 class="mb-0"> Points System </h3>
        </div>
        <div class="">
          <div class="pointSystem_content py-3 px-5">
            <div class="col-sm-12">
              <div class="">
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#T20">T20</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" href="#ODI">ODI</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" href="#TEST">TEST</a>
                  </li>
                </ul>

                <div class="tab-content">

                  <div id="T20" class="tab-pane active">
                    <div class="pb-3">
                      <h4 class="accordion" data-toggle="collapse" data-target="#batPointTab"><a href="javascript:;"><img src="assets/img/cricket.svg" class="img-fluid" width="20">Batting </a></h4>
                      <div class="custom_scroll" id="batPointTab">
	                    <div class="pointHead">
	                      <ul>
                            <li>Type Of points</li>
                        	<li>T20</li>
	                      </ul>
	                    </div>
                        <div class="pointBody">
                          <ul>
                            <li>For every 4 hit</li>
                            <li>1</li>
                          </ul>
	                      <ul>
	                        <li>For being part of the starting XI</li>
	                        <li>2</li>
	                      </ul>
                          <ul>
                            <li>For every 6 hit</li>
                            <li>2</li>
                          </ul>
                          <ul>
                            <li>For 50 runs</li>
                            <li>4</li>
                          </ul>
                          <ul>
                            <li>Duck (EXCEPT BOWLER)</li>
                            <li>-5</li>
                          </ul>
                          <ul>
                            <li>For every run scored</li>
                            <li>1</li>
                          </ul>
                          <ul>
                            <li>For 100 runs</li>
                            <li>8</li>
                          </ul>
                          <ul>
                            <li>Strike rate 0 to 49.99</li>
                            <li>-4</li>
                          </ul>
                          <ul>
                            <li>Strike rate 100 to 149.99</li>
                            <li>5</li>
                          </ul>
                          <ul>
                            <li>Strike rate 150.00 to 199.99</li>
                            <li>10</li>
                          </ul>
                          <ul>
                            <li>Strike rate 200.00 or more</li>
                            <li>15</li>
                          </ul>
                          <ul>
                            <li>Strike rate 50 to 74.99</li>
                            <li>-5</li>
                          </ul>
                          <ul>
                            <li>Strike rate 75.00 to 99.99</li>
                            <li>-6</li>
                          </ul>
                          <ul>
                            <li>A player must score a minimum balls to be awarded the Strike Rate <br> bonus (EXCEPT BOWLER)</li>
                            <li>15</li>
                          </ul>
                        </div>
                      </div>

                      <h4 class="bowl accordion" data-toggle="collapse" data-target="#bowlPointTab"><a href="javascript:;"><img src="assets/img/basketball-ball.svg" class="img-fluid" width="20">Bowling </a></h4>
                      <div class="collapse custom_scroll" id="bowlPointTab">
	                    <div class="pointHead">
	                      <ul>
	                        <li>Type Of points</li>
	                        <li>T20</li>
	                      </ul>
	                    </div>
                        <div class="pointBody">
                          <ul>
                            <li>For every wicket taken</li>
                            <li>10</li>
                          </ul>
                          <ul>
                            <li>For 3-wicket</li>
                            <li>4</li>
                          </ul>
                          <ul>
                            <li>For every maiden</li>
                            <li>4</li>
                          </ul>
                          <ul>
                            <li>For 4-wicket</li>
                            <li>4</li>
                          </ul>
                          <ul>
                            <li>For 5-wicket</li>
                          <li>8</li>
                          </ul>
                          <ul>
                            <li>Economy rate 0 to 5.00</li>
                            <li>4</li>
                          </ul>
                          <ul>
                            <li>Economy rate 10.01 to 12.00</li>
                            <li>-6</li>
                          </ul>
                          <ul>
                            <li>Economy rate 12.01 or more</li>
                            <li>-8</li>
                          </ul>
                          <ul>
                            <li>A player must bowl a minimum over to be awarded the Economy Rate bonus</li>
                            <li>10</li>
                          </ul>
                        </div>
                      </div>

                      <h4 class="field accordion" data-toggle="collapse" data-target="#fieldPointTab"><a href="javascript:;"><i class="fa fa-running"></i>Fielding </a></h4>
                      <div class="collapse custom_scroll" id="fieldPointTab">
                        <div class="pointHead">
                          <ul>
                            <li>Type Of points</li>
                            <li>T20</li>
                          </ul>
                        </div>
                        <div class="pointBody">
                          <ul>
                            <li>Stumping</li>
                            <li>6</li>
                          </ul>
                          <ul>
                            <li>Run-out</li>
                            <li>6</li>
                          </ul>
                          <ul>
                            <li>Catch</li>
                            <li>4</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="ODI" class="tab-pane">
                    <div class="pb-3">
                      <h4 class="accordion" data-toggle="collapse" data-target="#batPointTab"><a href="javascript:;"><img src="assets/img/cricket.svg" class="img-fluid" width="20">Batting </a></h4>
                      <div class="custom_scroll" id="batPointTab">
	                      <div class="pointHead">
	                        <ul>
	                          <li>Type Of points</li>
	                          <li>ODI</li>
	                        </ul>
	                      </div>
	                      <div class="pointBody">
	                        <ul>
	                          <li>For every 4 hit</li>
	                          <li>1</li>
	                        </ul>
	                        <ul>
	                          <li>For being part of the starting XI</li>
	                          <li>2</li>
	                        </ul>
	                        <ul>
	                          <li>For every 6 hit</li>
	                          <li>2</li>
	                        </ul>
	                        <ul>
	                          <li>For 50 runs</li>
	                          <li>4</li>
	                        </ul>
	                        <ul>
	                          <li>For 200 runs</li>
	                          <li>40</li>
	                        </ul>
	                        <ul>
	                          <li>For 150 runs</li>
	                          <li>30</li>
	                        </ul>
	                        <ul>
	                          <li>Duck (EXCEPT BOWLER)</li>
	                          <li>-5</li>
	                        </ul>
	                        <ul>
	                          <li>For every run scored</li>
	                          <li>1</li>
	                        </ul>
	                        <ul>
	                          <li>For 100 runs</li>
	                          <li>8</li>
	                        </ul>
	                        <ul>
	                          <li>Strike rate 0 to 49.99</li>
	                          <li>-4</li>
	                        </ul>
	                        <ul>
	                          <li>Strike rate 100 to 149.99</li>
	                          <li>5</li>
	                        </ul>
	                        <ul>
	                          <li>Strike rate 150.00 to 199.99</li>
	                          <li>10</li>
	                        </ul>
	                        <ul>
	                          <li>Strike rate 200.00 or more</li>
	                          <li>15</li>
	                        </ul>
	                        <ul>
	                          <li>A player must score a minimum balls to be awarded the Strike Rate <br> bonus (EXCEPT BOWLER)</li>
	                          <li>15</li>
	                        </ul>
	                      </div>
                      </div>

                      <h4 class="bowl accordion" data-toggle="collapse" data-target="#bowlPointTab"><a href="javascript:;"><img src="assets/img/basketball-ball.svg" class="img-fluid" width="20">Bowling </a></h4>
                      <div class="collapse custom_scroll" id="bowlPointTab">
	                      <div class="pointHead">
	                        <ul>
	                          <li>Type Of points</li>
	                          <li>ODI</li>
	                        </ul>
	                      </div>
	                      <div class="pointBody">
	                        <ul>
	                            <li>For every wicket taken</li>
	                            <li>10</li>
	                        </ul>
	                        <ul>
	                          <li>For 7 wickets</li>
	                          <li>40</li>
	                        </ul>
	                        <ul>
	                          <li>For 3-wicket</li>
	                          <li>4</li>
	                        </ul>
	                        <ul>
	                          <li>For 6-wicket</li>
	                          <li>30</li>
	                        </ul>
	                        <ul>
	                          <li>For every maiden</li>
	                          <li>4</li>
	                        </ul>
	                        <ul>
	                          <li>For 4-wicket</li>
	                          <li>4</li>
	                        </ul>
	                        <ul>
	                          <li>For 5-wicket</li>
	                          <li>8</li>
	                        </ul>
	                        <ul>
	                          <li>Economy rate 7.01 to 10.00</li>
	                          <li>-5</li>
	                        </ul>
	                        <ul>
	                          <li>Economy rate 0 to 5.00</li>
	                          <li>4</li>
	                        </ul>
	                        ><ul>
	                          <li>Economy rate 10.01 to 12.00</li>
	                          <li>-6</li>
	                        </ul>
	                        <ul>
	                          <li>Economy rate 12.01 or more</li>
	                          <li>-8</li>
	                        </ul>
	                        <ul>
	                          <li>Economy rate 5.01 to 7.00</li>
	                          <li>-4</li>
	                        </ul>
	                        <ul>
	                          <li>A player must bowl a minimum over to be awarded the Economy Rate bonus</li>
	                          <li>20</li>
	                        </ul>
	                      </div>
	                  </div>

                      <h4 class="field accordion" data-toggle="collapse" data-target="#fieldPointTab"><a href="javascript:;"><i class="fa fa-running"></i>Fielding </a></h4>
                      <div class="collapse custom_scroll" id="fieldPointTab">
	                      <div class="pointHead">
	                        <ul>
	                            <li>Type Of points</li>
	                            <li>ODI</li>
	                        </ul>
	                      </div>
	                      <div class="pointBody">
	                        <ul>
	                            <li>Stumping</li>
	                            <li>6</li>
	                        </ul>
	                        <ul>
	                          <li>Run-out</li>
	                          <li>6</li>
	                        </ul>
	                        <ul>
	                          <li>Catch</li>
	                          <li>4</li>
	                        </ul>  
	                      </div>
	                  </div>
                  	</div>
                  </div>

                  <div id="TEST" class="tab-pane">
                    <div class="pb-3">
                      <h4 class="accordion" data-toggle="collapse" data-target="#batPointTab"><a href="javascript:;"><img src="assets/img/cricket.svg" class="img-fluid" width="20">Batting </a></h4>
                      <div class="custom_scroll" id="batPointTab">
	                      <div class="pointHead">
	                        <ul>
	                          <li>Type Of points</li>
	                          <li>TEST</li>
	                        </ul>
	                      </div>
	                      <div class="pointBody">
	                        <ul>
	                          <li>For every 4 hit</li>
	                          <li>1</li>
	                        </ul>
	                        <ul>
	                          <li>For being part of the starting XI</li>
	                          <li>2</li>
	                        </ul>
	                        <ul>
	                          <li>For every 6 hit</li>
	                          <li>3</li>
	                        </ul>
	                        <ul>
	                          <li>For 50 runs</li>
	                          <li>4</li>
	                        </ul>
	                        <ul>
	                          <li>For 300 runs or more</li>
	                          <li>40</li>
	                        </ul>
	                        <ul>
	                          <li>For 200 runs</li>
	                          <li>30</li>
	                        </ul>
	                        <ul>
	                          <li>For 150 runs</li>
	                          <li>25</li>
	                        </ul>
	                        <ul>
	                          <li>Duck (EXCEPT BOWLER)</li>
	                          <li>-5</li>
	                        </ul>
	                        <ul>
	                          <li>For every run scored</li>
	                          <li>1</li>
	                        </ul>
	                        <ul>
	                          <li>For 100 runs</li>
	                          <li>8</li>
	                        </ul>
	                        <ul>
	                          <li>A player must score a minimum balls to be awarded the Strike Rate <br> bonus (EXCEPT BOWLER)</li>
	                          <li>15</li>
	                        </ul>
	                      </div>
	                  </div>

                      <h4 class="bowl accordion" data-toggle="collapse" data-target="#bowlPointTab"><a href="javascript:;"><img src="assets/img/basketball-ball.svg" class="img-fluid" width="20">Bowling </a></h4>
                      <div class="collapse custom_scroll" id="bowlPointTab">
	                      <div class="pointHead">
	                      <ul>
	                        <li>Type Of points</li>
	                        <li>TEST</li>
	                      </ul>
	                      </div>
	                      <div class="pointBody">
	                        <ul>
	                          <li>For every wicket taken</li>
	                          <li>10</li>
	                        </ul>
	                        <ul>
	                          <li>For 7 wickets</li>
	                          <li>30</li>
	                        </ul>
	                        <ul>
	                          <li>For 3-wicket</li>
	                          <li>4</li>
	                        </ul>
	                        <ul>
	                          <li>For 6-wicket</li>
	                          <li>25</li>
	                        </ul>
	                        <ul>
	                          <li>For 4-wicket</li>
	                          <li>4</li>
	                        </ul>
	                        <ul>
	                            <li>For 5-wicket</li>
	                            <li>8</li>
	                        </ul>
	                        <ul>
	                          <li>For 8 wickets or more</li>
	                          <li>40</li>
	                        </ul>  
	                      </div>
	                  </div>

                      <h4 class="field accordion" data-toggle="collapse" data-target="#fieldPointTab"><a href="javascript:;"><i class="fa fa-running"></i>Fielding </a></h4>
                      <div class="collapse custom_scroll" id="fieldPointTab">
	                      <div class="pointHead">
	                        <ul>
	                          <li>Type Of points</li>
	                          <li>TEST</li>
	                        </ul>
	                      </div>
	                      <div class="pointBody">
	                        <ul>
	                          <li>Stumping</li>
	                          <li>6</li>
	                        </ul>
	                        <ul>
	                          <li>Run-out</li>
	                          <li>6</li>
	                        </ul>
	                        <ul>
	                          <li>Catch</li>
	                          <li>4</li>
	                        </ul>
	                      </div>
	                  </div>
                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include('footer.php');?>
